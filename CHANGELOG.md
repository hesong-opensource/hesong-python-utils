# CHANGELOG

## 1.1

🗓 2019-07-23

- change:
  - Integrate `setuptools_scm` for version management
- fix:
  - An error that `*.yml` files not be recognized as YAML files
- new:
  - A `pip` requirement file for development
